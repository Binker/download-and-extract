# Download and Extract

> Generic role to download over http(s) and unzip the downloaded files.  
> Note! Depends on being fed variables from the main Playbook.  


_____
# Important!
To provide this role with the neccessary variables, your main Playbook needs to reference them.  
In your main Playbook, do someting like:  
```yaml
- include_role:
    name: download-and-extract
    public: True    # Needed to provide main Play with info about downloaded files
    vars_from: "{{ playbook_dir  }}/path/to/variable.yml"  # Variables this role needs
```
Alternatively, you could do something like:  
```yaml
- include_role:
    name: download-and-extract
  vars:
    dl_url: "xxxx"
    dl_dest: "xxxx"
    file_ext: 
      - zip
```

The variable file needs to contain the following:  
-    dl_url: "\<source url\>" - Full url to the file(s) you'll be downloading.  
-    dl_dest: "\<destination on target host\>" - Directory path to where the file(s) will be downloaded to.  
-    file_ext:  *# Needs to be a list of one or more extensions*
      - zip
      - exe
      - etc...  

_____

## Requirements for this role

### Usage:
In order to use this generic roles, your Playbooks must meet the following requirements:  
A file name "requirements.yml" must exist in the relative path:  
`./roles/requirements.yml`  
The "requirements.yml" file should look like this:  
```yaml
---
  # REQUIREMENTS | Locate Oracle Middleware Home
  - src: https://gitlab.com/Binker/download-and-extract.git
    scm: git
    name: download-and-extract
    version: master
    validate_certs: True
```
You can run this command inside your Playbook directory to install it:  
```bash
$ ansible-galaxy install -r roles/requirements.yml --roles-path=./roles
```

## Optional auto-install
To automatically use this role in your Play, make sure that you reference a file in your Playbook to install this role.  
The advantage of this is that you get the latest available version with each run.  
The drawback, naturally, is that it takes a little longer to run the Play as it has to download and install the role with each run.  
This file can reside anywhere inside your directory structure as long as it's referenced with the relative root path of the Play.  
Example:
```yaml
  pre_tasks:
    - import_tasks: install_roles.yml
```
##### NOTE!  
The above file must be run in a separate, first Play like this:
```yaml
---
- name: Install roles
  hosts: localhost
  gather_facts: False
  become: False # Negates any privilege escalation set in the ansible.cfg file
  pre_tasks:
    - import_tasks: install_roles.yml
```
In the same file (typically "./main.yml"), continue with the rest of the Play like this:  
```yaml
- name: Some sensible name
  hosts: all
  pre_tasks:
    - include_role:
        name: download-and-extract
  roles:
    - role: some_other_role
```
The "install_roles.yml" file should like this:  
```yaml
---
  - name: INSTALL ROLES | Clear out the generic role directory
    file:
      path: "./roles/{{ item }}"
      state: absent
    loop:
      - download-and-extract


  - name: INSTALL ROLES | Use ansible-galaxy command to install role from GitLab
    local_action: "command ansible-galaxy install -r roles/requirements.yml --roles-path=./roles"

  - name: INSTALL ROLES | Make exception for the project(s) in .gitignore
    lineinfile:
      dest: .gitignore
      regexp: '^\/roles$'
      line: 'roles/{{ item }}/'
      state: present
      create: True
    loop:
      - download-and-extract

```



